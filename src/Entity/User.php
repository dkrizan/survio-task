<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidV4Generator;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\UuidV4;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface {

    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidV4Generator::class)
     */
    private $id;

    /**
     * User full name
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * User login
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $login;

    /**
     * User password
     *
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * User's projects
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="user")
     * @var Project[]|ArrayCollection
     */
    private $projects;

    public function getId(): UuidV4 {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(?string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getLogin(): ?string {
        return $this->login;
    }

    public function setLogin(string $login): self {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string {
        return $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Project[]|ArrayCollection
     */
    public function getProjects() {
        return $this->projects;
    }

    public function getRoles() {
        return ['ROLE_USER'];
    }

    public function getSalt() {
        return null;
    }

    public function getUsername() {
        return $this->login;
    }

    public function eraseCredentials() {

    }
}
