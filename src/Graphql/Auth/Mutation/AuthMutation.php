<?php
/**
 * Created by PhpStorm.
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 * Date: 13.02.21 21:03
 */

namespace App\Graphql\Auth\Mutation;


use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Overblog\GraphQLBundle\Error\UserError;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthMutation implements MutationInterface, AliasedInterface {

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * PostMutation constructor.
     * @param EntityManagerInterface $em
     * @param UserRepository $userRepository
     * @param JWTTokenManagerInterface $jwtManager
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(
        EntityManagerInterface $em,
        UserRepository $userRepository,
        JWTTokenManagerInterface $jwtManager,
        UserPasswordEncoderInterface $encoder
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->jwtManager = $jwtManager;
        $this->encoder = $encoder;
    }

    /**
     * Mutation - signup
     *
     * @param string $login
     * @param string $password
     * @param string|null $name
     * @return array
     */
    public function signup(string $login, string $password, ?string $name) : array {
        $user = $this->userRepository->findOneBy(['login' => $login]);

        if ($user) {
            throw new UserError('User with this login already exists !');
        }

        $user = new User();
        $user->setLogin($login);
        $user->setPassword($this->encoder->encodePassword($user, $password));
        $user->setName($name);
        $this->em->persist($user);
        $this->em->flush();

        return ['token' => $this->jwtManager->create($user)];
    }

    /**
     * Mutation - login
     *
     * @param string $login
     * @param string $password
     * @return array
     */
    public function login(string $login, string $password) : array {
        $user = $this->userRepository->findOneBy(['login' => $login]);
        if (!$user || !$this->encoder->isPasswordValid($user, $password)) {
            throw new UserError('Invalid credentials.');
        }
        return ['token' => $this->jwtManager->create($user)];
    }

    public static function getAliases(): array {
        return [
            'signup' => 'signupUser',
            'login' => 'loginUser'
        ];
    }
}