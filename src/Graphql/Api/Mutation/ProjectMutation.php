<?php
/**
 * Created by PhpStorm.
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 * Date: 13.02.21 21:03
 */

namespace App\Graphql\Api\Mutation;


use App\Entity\Project;
use App\Repository\ProjectRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Error\UserError;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ProjectMutation implements MutationInterface, AliasedInterface {

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorageInterface;

    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * PostMutation constructor.
     * @param EntityManagerInterface $em
     * @param UserRepository $userRepository
     * @param ProjectRepository $projectRepository
     * @param TokenStorageInterface $tokenStorageInterface
     * @param JWTTokenManagerInterface $jwtManager
     */
    public function __construct(
        EntityManagerInterface $em,
        UserRepository $userRepository,
        ProjectRepository $projectRepository,
        TokenStorageInterface $tokenStorageInterface,
        JWTTokenManagerInterface $jwtManager
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->jwtManager = $jwtManager;
        $this->tokenStorageInterface = $tokenStorageInterface;
        $this->projectRepository = $projectRepository;
    }

    /**
     * Mutation - createProject
     *
     * @param string $name
     * @param string $description
     * @return Project
     */
    public function create(string $name, string $description) : Project {
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $user = $this->userRepository->findOneBy(['login' => $decodedJwtToken["login"]]);
        if (!$user) {
            throw new UserError('User not found');
        }

        $project = new Project();
        $project->setName($name);
        $project->setDescription($description);
        $project->setUser($user);
        $this->em->persist($project);
        $this->em->flush();

        return $project;
    }

    /**
     * Mutation - updateProject
     *
     * @param string $id
     * @param string $name
     * @param string $description
     * @return Project
     */
    public function update(string $id, string $name, string $description) : Project {
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $user = $this->userRepository->findOneBy(['login' => $decodedJwtToken["login"]]);
        if (!$user) {
            throw new UserError('User not found');
        }
        $project = $this->projectRepository->find($id);
        if (!$project) {
            throw new UserError('Project not found.');
        }
        if ($project->getUser() !== $user) {
            throw new UserError('You are not allowed to edit others projects.');
        }
        $project->setName($name);
        $project->setDescription($description);
        $this->em->flush();

        return $project;
    }

    /**
     * Mutation - deleteProject
     *
     * @param string $id
     * @return string[]
     */
    public function delete(string $id) : array {
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $user = $this->userRepository->findOneBy(['login' => $decodedJwtToken["login"]]);

        $project = $this->projectRepository->find($id);
        if (!$project) {
            throw new UserError('Project not found.');
        }
        if ($project->getUser() !== $user) {
            throw new UserError('You are not allowed to delete others projects.');
        }

        $this->em->remove($project);
        $this->em->flush();

        return ["status" => "ok"];
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases() : array {
        return [
            'create' => 'createProject',
            'update' => 'updateProject',
            'delete' => 'deleteProject'
        ];
    }
}