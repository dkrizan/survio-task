<?php
/**
 * Created by PhpStorm.
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 * Date: 13.02.21 20:16
 */

namespace App\Graphql\Api\Resolver;


use App\Entity\Project;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Error\UserError;
use GraphQL\Type\Definition\ResolveInfo;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ProjectResolver implements ResolverInterface {

    /**
     * @var EntityManagerInterface
     */
    protected $em;
    protected $jwtManager;
    protected $tokenStorageInterface;
    protected $userRepository;

    /**
     * UserResolver constructor.
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface $tokenStorageInterface
     * @param JWTTokenManagerInterface $jwtManager
     * @param UserRepository $userRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorageInterface,
        JWTTokenManagerInterface $jwtManager,
        UserRepository $userRepository
    ) {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->tokenStorageInterface = $tokenStorageInterface;
        $this->userRepository = $userRepository;

    }

    /**
     * @param ResolveInfo $info
     * @param $value
     * @param Argument $args
     * @return mixed
     */
    public function __invoke(ResolveInfo $info, $value, Argument $args) {
        $method = $info->fieldName;
        return $this->$method($value, $args);
    }

    /**
     * @param string $id
     * @return Project
     */
    public function resolve(string $id) : Project {
        $project =  $this->em->getRepository(Project::class)->findOneBy(['id' => $id]);
        if (!$project) {
            throw new UserError('Project not found.');
        }
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $user = $this->userRepository->findOneBy(['login' => $decodedJwtToken["login"]]);
        if ($project->getUser() !== $user) {
            throw new UserError('You are not allowed to see others projects.');
        }
        return $project;
    }

    /**
     * @param Project $project
     * @return string
     */
    public function name(Project $project) :string {
        return $project->getName();
    }


    /**
     * @param Project $project
     * @return string
     */
    public function description(Project $project) :string {
        return $project->getDescription();
    }
}