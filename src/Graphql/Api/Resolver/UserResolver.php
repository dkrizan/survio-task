<?php
/**
 * Created by PhpStorm.
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 * Date: 13.02.21 20:16
 */

namespace App\Graphql\Api\Resolver;


use App\Entity\Project;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserResolver implements ResolverInterface {

    /**
     * @var EntityManagerInterface
     */
    protected $em;
    protected $jwtManager;
    protected $tokenStorageInterface;
    protected $userRepository;

    /**
     * UserResolver constructor.
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface $tokenStorageInterface
     * @param JWTTokenManagerInterface $jwtManager
     * @param UserRepository $userRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorageInterface,
        JWTTokenManagerInterface $jwtManager,
        UserRepository $userRepository
    ) {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->tokenStorageInterface = $tokenStorageInterface;
        $this->userRepository = $userRepository;

    }

    /**
     * @param ResolveInfo $info
     * @param $value
     * @param Argument $args
     * @return mixed
     */
    public function __invoke(ResolveInfo $info, $value, Argument $args) {
        $method = $info->fieldName;
        return $this->$method($value, $args);
    }

    /**
     * @return User
     */
    public function resolve() {
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        return $this->em->getRepository(User::class)->findOneBy(['login' => $decodedJwtToken['login']]);
    }

    /**
     * @param User $user
     * @return string
     */
    public function login(User $user) :string {
        return $user->getLogin();
    }

    /**
     * @param User $user
     * @return string
     */
    public function name(User $user) :string {
        return $user->getName();
    }

    /**
     * @param User $user
     * @param Argument $args
     * @return Project[]
     */
    public function projects(User $user, Argument $args) {
        return $user->getProjects();
    }
}