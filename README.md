Survio Task
=======
Installation
----
1. Create `.env.local` from `.env` configuration.


2. Run docker containers

`docker-compose up`

3. Install dependencies with composer

`composer install`

4. Run migrations

`php bin/console doctrine:migrations:migrate`

and thats it.

GraphQL API
-------

- **auth** schema
    - endpoint: /graphql/auth 
    - mutations: signup, login
- **api** schema
    - endpoint: /graphql/api
    - mutations: createProject, updateProject, deleteProject
    - queries: me, project
