<?php
/**
 * Created by PhpStorm.
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 * Date: 14.02.21 19:11
 */

namespace App\Tests;


use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use \Symfony\Component\HttpFoundation\Response;

class TestCase extends WebTestCase {

    const API_PREFIX = "/graphql";

    /** @var UserRepository */
    protected $users;

    /** @var string */
    private $token;

    /** @var EntityManager */
    protected $em;

    /** @var \Symfony\Bundle\FrameworkBundle\KernelBrowser */
    protected $client;

    protected function setUp(): void {
        parent::setUp();
        $this->client = self::createClient();
        $this->em = $this->client->getContainer()->get('doctrine')->getManager();
        $this->users = $this->em->getRepository(User::class);
    }

    /**
     * Sends POST request to API
     *
     * @param $endpoint
     * @param string $query
     * @param array $variables
     * @param bool $authorized
     * @return Response
     */
    protected function request($endpoint, string $query, $variables = [], $authorized = true) : Response {
        $data = ['query' => $query, 'variables' => $variables];
        $header = ['CONTENT_TYPE' => 'application/json'];
        if ($authorized) {
            $header['HTTP_AUTHORIZATION'] = 'Bearer ' . $this->getToken();
        }
        $this->client->request(
            'POST',
            self::API_PREFIX . $endpoint,
            [],
            [],
            $header,
            json_encode($data)
        );
        return $this->client->getResponse();
    }

    /**
     * Login user and set token
     * @return string
     */
    private function getToken(): string {
        if (!$this->token) {
            $user = $this->users->findOneBy(['login' => 'user']);
            if (!$user) {
                $user = new User();
                $user->setLogin('user');
                $encoder = $this->client->getContainer()->get("security.password_encoder");
                $user->setPassword($encoder->encodePassword($user, 'user'));
                $this->em->persist($user);
                $this->em->flush();
            }

            $response = $this->request(
                '/auth',
                'mutation login($user: LoginUserInput!) {
                    login(input: $user) {
                           token
                    }
                }',
                ['user' => [
                    'login' => 'user',
                    'password' => 'user'
                ]],
                false
            );

            $data = json_decode($response->getContent(), true);
            $this->token = $data['data']['login']['token'];
        }
        return $this->token;
    }
}