<?php
/**
 * Created by PhpStorm.
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 * Date: 14.02.21 17:33
 */

namespace App\Tests\Mutation;


use App\Tests\TestCase;

class AuthMutationTest extends TestCase {

    public function testSignup() {
        $user = $this->users->findOneBy(['login' => 'test']);
        if ($user) {
            $this->em->remove($user);
            $this->em->flush();
        }

        $response = $this->request(
            '/auth',
            'mutation signup($user: SignupUserInput!) {
                signup(input: $user) {        
                    token    
                }
            }',
            ['user' => [
                'login' => 'test',
                'password' => 'test',
                'name' => 'Test test'
            ]],
            false
        );

        $data = json_decode($response->getContent(), true);

        $this->assertNotEmpty($data['data']['signup']['token']);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testLogin() {
        $response = $this->request(
            '/auth',
            'mutation login($user: LoginUserInput!) {
                login(input: $user) {
                       token
                }
            }',
            ['user' => [
                'login' => 'test',
                'password' => 'test'
            ]],
            false
        );

        $data = json_decode($response->getContent(), true);

        $this->assertNotEmpty($data['data']['login']['token']);
        $this->assertEquals(200, $response->getStatusCode());
    }
}