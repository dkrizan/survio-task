<?php
/**
 * Created by PhpStorm.
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 * Date: 14.02.21 17:33
 */

namespace App\Tests\Mutation;


use App\Entity\Project;
use App\Repository\ProjectRepository;
use App\Tests\TestCase;

class ApiMutationTest extends TestCase {

    /** @var ProjectRepository */
    private $projects;

    protected function setUp(): void {
        parent::setUp();
        $this->projects = $this->em->getRepository(Project::class);
    }

    public function testCreateProject() {
        $response = $this->request(
            '/api',
            'mutation createProject($project: CreateProjectInput!) {
                createProject(input: $project) {
                    name,
                    description
                }
            }',
            ['project' => [
                'name' => 'New project',
                'description' => 'Description'
            ]]
        );

        $data = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertSame('New project', $data['data']['createProject']['name']);
        $this->assertSame('Description', $data['data']['createProject']['description']);
    }

    public function testUpdateProject() {
        $project = $this->createProject('Project2', 'Description2');
        $response = $this->request(
            '/api',
            'mutation updateProject($project: UpdateProjectInput!) {
                updateProject(input: $project) {
                    name,
                    description
                }
            }',
            ['project' => [
                'id' => $project->getId(),
                'name' => 'Updated project',
                'description' => 'Updated desc'
            ]]
        );

        $data = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertSame('Updated project', $data['data']['updateProject']['name']);
        $this->assertSame('Updated desc', $data['data']['updateProject']['description']);
    }

    public function testDeleteProject() {
        $project = $this->createProject('Project3', 'Description3');
        $response = $this->request(
            '/api',
            'mutation deleteProject($project: DeleteProjectInput!) {
                deleteProject(input: $project) {
                    status
                }
            }',
            ['project' => [
                'id' => $project->getId()
            ]]
        );

        $data = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertSame('ok', $data['data']['deleteProject']['status']);
    }

    private function createProject(string $name, string $desc) : Project {
        $project = new Project();
        $project->setName($name);
        $project->setDescription($desc);
        $project->setUser($this->users->findOneBy(['login' => 'user']));
        $this->em->persist($project);
        $this->em->flush();
        return $project;
    }
}